Ubercart Cart Manager is a simple module that allows authorized users to view
the contents of customers' shopping carts. Besides the 'administer user carts'
permission, there are no configuration settings, and there are no actual cart
management features yet. The cart manager can be found at Administer » Store
administration » Customers » Cart manager.
